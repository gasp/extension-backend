// exchange with extension

window.extension = {};
extension.version = '0.1.1';
// status list
/*
  - off = not installed
  - enabled = installed
  - idle = installed but doing nothing
  - active = installed and clicked so ready
  - recording = currently recording
*/
extension.status = "off";
extension.statusSet = function (status) {
  this.status = status;
  sessionStorage.setItem('videodesk_extension_status', status);
  $("#extension-status").text(status);
};
extension.statusCtr = function (status) {
  this.statusSet(status);
  if(status === "enabled")
    extension.modal("show");
  if(status === "active")
    extension.modal("hide");
  console.log(status);
};

// set up listeners
extension.init = function () {
  var self = this;
  this.configure();
  window.addEventListener("message", function (event) {
    // TODO add more security here
    // ignoring unformatted events
    if(!event.data)
      return;
    // catching events addressed to the webpage
    if(typeof(event.data.to) !== "undefined" && event.data.to === "webpage") {
      self.listen(event);
      return;
    }
    // this event is garbage for the webpage
    console.info('extension.js ignored event', event)
  });
};

extension.config = {};

extension.configure = function() {
  // get configuration from
  // videodesk_config_set_url
  // videodesk_config_set_token
  /// or directly from the dom
  // the url should be allowed in the manifest
  this.config.url = 'http://extension-client.dev/uploader.php';
  this.config.token = "abcdefgh0!xGyWzci.SaE";
  console.log(this);
};


extension.query = function () {
  var queryMessage = {
    type: "videodesk_extension_query",
    to: "contentScript"
  };
  window.postMessage(queryMessage, self.location.href);
};

extension.listen = function (event) {
  console.log("extension.js got message", event.data);

  // getting status
  if(event.data && event.data.type === "videodesk_extension_status") {
    if(typeof(event.data.status) === "undefined")
      throw "missing status in a videodesk_extension_status event";
    extension.statusCtr(event.data.status);
  }

  // getting upload status
  if(event.data && event.data.type === "videodesk_upload_status") {
    if(typeof(event.data.status) === "undefined")
      throw "GFY";
    console.log("**************************");
    console.log(event.data);
  }

  // getting file list
  if(event.data && event.data.type === "videodesk_file_list") {
    var notuploaded = {files: 0, size: 0};
    for (var i = event.data.list.length - 1; i >= 0; i--) {
      notuploaded.files ++;
      notuploaded.size += event.data.list[i].size;
    };
    console.log(notuploaded.files, 'files for ', notuploaded.size, 'o.');
  }
};

extension.modal = function (action) {
  if(action == 'show') {
    $('#modal-extension').modal({
      keyboard: false,
      backdrop: 'static',
      show: true
    });
  }
  else {
    $('#modal-extension').modal({
      show: false
    });
    $('.modal-backdrop').hide();
  }
};

// @param action (str) start or stop
// @param call_type (str) video, audio, whatever...
// @param call_uid (str) '1234567890ABCDEF'
extension.call = function (options) {
  if(!options.action || !options.call_uid)
    throw "extension.call: you have to specify an action and a call_uid";
  if(options.action === "start" && !options.call_type)
  throw "extension.call: you have to specify a call_type";
  var call_message = {
    type: "videodesk_call",
    to: "contentScript",
    call_type: options.call_type,
    action: options.action,
    call_uid: options.call_uid
  };
  window.postMessage(call_message, self.location.href);
};

extension.upload = function() {
  var upload_message = {
    type: "videodesk_upload",
    to: "contentScript",
    action: "start",
    url: this.config.url,
    token: this.config.token
  };
  console.log(upload_message);
  window.postMessage(upload_message, self.location.href);
};

extension.list = function() {
  var list_message = {
    type: "videodesk_upload",
    to: "contentScript",
    action: "list"
  };
  window.postMessage(list_message, self.location.href);
};

extension.init();

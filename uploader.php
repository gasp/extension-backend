<?php

require(dirname(__FILE__).DIRECTORY_SEPARATOR.'secure.php');

// relative upload dir should start and end with a /
define('SCREENRECORD_UPLOAD_DIR', DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR);
define('SCREENRECORD_UPLOAD_CHMOD', '766');

$message = [];
$message['debug'] = $_FILES;
$message['version'] = '0.6';
$message['config_upload_max_filesize'] = ini_get('upload_max_filesize');
$message['config_post_max_size'] = ini_get('post_max_size');

$uploaddir = dirname(__FILE__) . SCREENRECORD_UPLOAD_DIR;


if($_SERVER['REQUEST_METHOD'] == 'GET'){
    $message['about'] = 'GET requests will soon be forbidden';
}
$headers = apache_request_headers();

// check security token
if(!isset($headers['token']) || !crypto_check($headers['token'])) {
    $message['result'] = false;
    $message['hint'] = 'Invalid token';
    echo json_encode($message);
    exit();
}

// check writable
if(!file_exists($uploaddir) || !is_writable($uploaddir)) {
    $message['result'] = false;
    $message['hint'] = 'Directory is not writable';
    $message['comment'] = $uploaddir;
    echo json_encode($message);
    exit();
}

$origin = isset($_POST['origin'])?$_POST['origin'] : false;

$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
    chmod($uploadfile, SCREENRECORD_UPLOAD_CHMOD);
    $message['result'] = true;
    $message['hint'] = 'File is valid, and was successfully uploaded.';
    $message['origin'] =  $origin;
    $message['md5'] = md5($origin);

} else {
    $message['result'] = false;
    $message['hint'] = 'Did not store the file. Possible file upload attack!';
}


echo json_encode($message);
?>

<?php

define('CRYPTO_KEY', '0!$tc\+Ko^vSki$');

function crypto_check($token='') {
	$len = strlen($token);
	if($len < 14) {
		return false;
	}
	// identifyier is in the first 8 characters
	$id = substr($token, 0, 8);
	// key is in the other characters
	$key = substr($token, 8, strlen($token));
	// generate the correct key
	$cr = crypt($token, CRYPTO_KEY);
	return ($key == $cr);
}


function crypto_generate($key) {
	if(strlen($key) != 8) {
		return false;
	}
	return crypt($key, CRYPTO_KEY);
}